import { styled } from '@mui/material/styles';
import { Box, Container, Typography } from "@mui/material";
import MyAppBar from '../myAppBar'
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import IconButton from '@mui/material/IconButton';
import { useRouter } from 'next/router';

const CustomContainer = styled(Container)`
    display: "flex",
    flexDirection: 'column',
    height: '100vh'
`;

export default function Page({ title, children, isHome = false }) {

  const router = useRouter();

  return (
    <>
      <MyAppBar/>
      <CustomContainer maxWidth="lg">  
        <Box my={6}>
          <Box display={"flex"} mb={3}>
            {!isHome && (
              <IconButton 
                color="primary" 
                size="large" 
                onClick={() => router.back()}
                style={{marginTop: 'auto', marginBottom: 'auto', marginRight: 10}}
              >
                <KeyboardBackspaceIcon fontSize='inherit'/>
              </IconButton>
            )}
            <Typography variant="h3" >{title}</Typography>
          </Box>
          {children}
        </Box>
      </CustomContainer>
    </>
  );
}