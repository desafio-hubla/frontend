import React from "react";
import Image from "next/image";
import styles from "../styles/DropZone.module.css";
import FilePreview from "../filePreview";
import { Alert, AlertTitle, CircularProgress, Fab } from "@mui/material";
import { Navigation as NavigationIcon } from '@mui/icons-material';
import { useRouter } from "next/router";
import { Box } from "@mui/system";

const DropZone = ({ data, dispatch }) => {

    const [ state, setState ] = React.useState({
        loading: false,
        showAlert: false
    })
 
    const router = useRouter();

    const handleDragEnter = (e) => {
        e.preventDefault();
        e.stopPropagation();
        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: true });
    };

    // onDragLeave sets inDropZone to false
    const handleDragLeave = (e) => {
        e.preventDefault();
        e.stopPropagation();

        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });
    };


    const handleDragOver = (e) => {
        e.preventDefault();
        e.stopPropagation();

        // set dropEffect to copy i.e copy of the source item
        e.dataTransfer.dropEffect = "copy";
        dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: true });
    };

    // onDrop sets inDropZone to false and adds files to fileList
    const handleDrop = (e) => {
        e.preventDefault();
        e.stopPropagation();

        // get files from event on the dataTransfer object as an array
        let files = [...e.dataTransfer.files];

        // ensure a file or files are dropped
        if (files && files.length > 0) {
            // loop over existing files
            const existingFiles = data.fileList.map((f) => f.name);
            // check if file already exists, if so, don't add to fileList
            // this is to prevent duplicates
            files = files.filter((f) => !existingFiles.includes(f.name));

            // dispatch action to add droped file or files to fileList
            dispatch({ type: "ADD_FILE_TO_LIST", files });
            // reset inDropZone to false
            dispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });
        }
    };

    const handleFileSelect = (e) => {
        // get files from event on the input element as an array
        let files = [...e.target.files];

        // ensure a file or files are selected
        if (files && files.length > 0) {
            // loop over existing files
            const existingFiles = data.fileList.map((f) => f.name);
            // check if file already exists, if so, don't add to fileList
            // this is to prevent duplicates
            files = files.filter((f) => !existingFiles.includes(f.name));

            // dispatch action to add selected file or files to fileList
            dispatch({ type: "ADD_FILE_TO_LIST", files });
        }
    };


    const uploadFiles = async () => {
        setState({loading: true})
        // get the files from the fileList as an array
        let files = data.fileList;
        // initialize formData object
        const formData = new FormData();
        // loop over files and add to formData
        files.forEach((file) => formData.append("files", file));

        // Upload the files as a POST request to the server using fetch
        // Note: /api/fileupload is not a real endpoint, it is just an example
        const response = await fetch("http://localhost:8080/api/v1", {
            method: "POST",
            body: formData
        });

        //successful file upload
        if (response.ok) {
            router.push('/products')
            setState({showAlert: true, success: true})
        } else {
            // unsuccessful file upload
            alert("Error uploading files");
            setState({showAlert: true, success: false})
        }

        setState({loading: false})
    };

    return (
        <>
            <div
                className={styles.dropzone}
                onDragEnter={(e) => handleDragEnter(e)}
                onDragOver={(e) => handleDragOver(e)}
                onDragLeave={(e) => handleDragLeave(e)}
                onDrop={(e) => handleDrop(e)}
            >
                <Image src="/upload.svg" alt="upload" height={50} width={50} />

                <input
                    id="fileSelect"
                    type="file"
                    multiple
                    disabled={state.loading}
                    className={styles.files}
                    onChange={(e) => handleFileSelect(e)}
                />
                <label htmlFor="fileSelect">You can select multiple Files</label>

                <h3 className={styles.uploadMessage}>
                    or drag &amp; drop your files here
                </h3>
            </div>
            <FilePreview fileData={data} />
            {data?.fileList.length > 0 && (
                <Box textAlign={"center"}>
                    <Fab variant="extended" onClick={uploadFiles} color="primary" disabled={state.loading}>
                        {state.loading?
                        <>
                            <CircularProgress style={{color: 'black', marginRight: 10}} size={20}/>
                            Uploading
                        </>
                        :
                        <>
                            <NavigationIcon sx={{ mr: 1 }} />
                            Upload
                        </>                        
                    }

                    </Fab>
                </Box>
            )}
            {state.showAlert && (
                <Box>
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        This is an error alert — <strong>check it out!</strong>
                    </Alert>
                </Box>
            )}
        </>
    );
};

export default DropZone;