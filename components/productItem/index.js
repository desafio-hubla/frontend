import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useRouter } from 'next/router';

export default function ProductItem({ product }) {

    const router = useRouter();

    return (
        <Card sx={{ margin: 'auto' }}>
            <CardMedia
                component="img"
                height="140"
                image="https://picsum.photos/300/200"
                alt="green iguana"
            />
            <CardContent>
                <Typography gutterBottom component="div">
                    {product.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    R$ {product.price}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={() => router.push(`/products/${product.id}`)}>Learn More</Button>
            </CardActions>
        </Card>
    );
}