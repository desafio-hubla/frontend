import * as React from 'react';
import Page from '../components/page';
import DropZone from '../components/drop/dropZone'

export default function Index() {

  // reducer function to handle state changes
  const reducer = (state, action) => {
    switch (action.type) {
      case "SET_IN_DROP_ZONE":
        return { ...state, inDropZone: action.inDropZone };
      case "ADD_FILE_TO_LIST":
        return { ...state, fileList: state.fileList.concat(action.files) };
      default:
        return state;
    }
  };

  // destructuring state and dispatch, initializing fileList to empty array
  const [data, dispatch] = React.useReducer(reducer, {
    inDropZone: false,
    fileList: [],
  });

  return (
    <Page title="Welcome to Hubla challenge!" isHome>
      <DropZone data={data} dispatch={dispatch}/>
    </Page>
  );
}
