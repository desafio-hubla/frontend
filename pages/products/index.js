import { Grid, Link } from "@mui/material";
import { useEffect, useState } from "react";
import Page from "../../components/page";
import ProductItem from "../../components/productItem";


const Index = () => {

    const [products, setProducts] = useState([])

    useEffect(() => {
        fetch("http://localhost:8080/api/v1")
        .then(response => response.json())
            // 4. Setting *dogImage* to the image url that we received from the response above
        .then(data => setProducts(data.products))
    },[])

    return (
        <Page title="Product list">
            <Grid container spacing={2} margin={'auto'} mt={'5vh'}>

                {products?.map((product) => (
                    <Grid item lg={3} md={4}>
                        <ProductItem
                            product={product}
                        />
                    </Grid>
                ))}

            </Grid>
        </Page>
    )
};

export default Index;