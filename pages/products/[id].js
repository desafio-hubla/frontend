import * as React from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import { Container } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Star from '@mui/icons-material/Star';
import { Box } from '@mui/system';
import TransactionTable from '../../components/transactionTable';
import Page from '../../components/page';

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, .05)'
      : 'rgba(0, 0, 0, .03)',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

function Transaction({id}) {
  const [expanded, setExpanded] = React.useState();

  const [transactionsBySeller, setTransactionsBySeller] = React.useState({})

    React.useEffect(() => {
        fetch(`http://localhost:8080/api/v1/${id}`)
        .then(response => response.json())
            // 4. Setting *dogImage* to the image url that we received from the response above
        .then(data => setTransactionsBySeller(data))
    },[])

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <Page title={"Transactions per user"}>
      <Container style={{ marginTop: '5vh' }}>
        <Typography variant='h2' style={{ marginBottom: '20px' }}></Typography>
        {Object.keys(transactionsBySeller).map((seller, index) => (
          <Accordion expanded={expanded === `panel${index}`} onChange={handleChange(`panel${index}`)}>
            <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
              <Box style={{ display: 'flex', width: '100%' }}>
                <Typography style={{ marginTop: 'auto', marginBottom: 'auto' }}>{seller}</Typography>
                <Typography>{transactionsBySeller[seller][0].seller.type == 0 ? <Star style={{ marginTop: 3 }} /> : null}</Typography>
                <Typography style={{ margin: 'auto', marginRight: 0, color: 'gray' }}>balance: R${transactionsBySeller[seller][0].seller.balance}</Typography>
              </Box>
            </AccordionSummary>
            <AccordionDetails>
              <TransactionTable transactions={transactionsBySeller[seller]} />
            </AccordionDetails>
          </Accordion>
        ))}
      </Container>
    </Page>
  );
}

export async function getServerSideProps(props) {
  // Fetch data from external API
  const { params: { id } } = props;

  // Pass data to the page via props
  return { props: { id } }
}


export default Transaction;