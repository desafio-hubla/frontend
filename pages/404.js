import { Typography, Button } from "@mui/material";
import { Box, Container } from "@mui/system";
import { useRouter } from "next/router";

export default function Custom404() {

    const router = useRouter();

    return (
        <Container >
            <Box textAlign="center" mt={"30vh"}>
                <Typography variant="h2">Oops, there is an error!</Typography>

                <Button variant="contained" onClick={() => router.push("/")}>
                    Main page
                </Button>
            </Box>
        </Container>
    )
}